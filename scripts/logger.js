﻿function getDateTime() {
    var date = new Date();
    
    var hour = date.getHours();
    hour = (hour < 10 ? "0" : "") + hour;
    
    var minute = date.getMinutes();
    minute = (minute < 10 ? "0" : "") + minute;
    
    var seconds = date.getSeconds();
    seconds = (seconds < 10 ? "0" : "") + seconds;
    
    var year = date.getFullYear();
    
    var month = date.getMonth() + 1;
    month = (month < 10 ? "0" : "") + month;
    
    var day = date.getDate();
    day = (day < 10 ? "0" : "") + day;
    
    return year + ":" + month + ":" + day + " " + hour + ":" + minute + ":" + seconds;
}

module.exports = function (app) {
    var logger = require('morgan');
    var fs = require('fs');

    var accessLogStream = fs.createWriteStream(__dirname + '/../logs/access.log', { flags: 'a' });
    accessLogStream.write(
        "#############################\n" +
        "####   Application Started\n" +
        "####   " + getDateTime() + "\n" +
        "#############################\n");
    app.use(logger('tiny', { stream: accessLogStream }));
    app.use(logger('dev'));
};

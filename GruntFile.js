﻿module.exports = function (grunt)
{
    grunt.initConfig({
        jshint: {
            options: {
                'curly': true,
                'eqnull': true,
                'eqeqeq': true,
                'undef': true,
                'unused': 'vars',
            },
            node: {
                options: {
                    'node': true
                },
                files: [
                    { src: 'app.js' },
                    { expand: true, src: ['routes/*.js'] }
                ]
            },
            tests: {
                options: {
                    'node': true,
                    'mocha': true
                },
                files: [
                    { expand: true, src: ['tests/**/*.js'] }
                ]
            }
        },
        mochaTest: {
            node: {
                options: {
                    ui: 'tdd',
                    captureFile: 'logs/test/node.log',
                    quiet: false
                },
                src: 'tests/node/*.js'
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-mocha-test');

    grunt.registerTask('default', ['jshint']);
    grunt.registerTask('test', ['mochaTest']);
}

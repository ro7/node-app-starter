﻿var assert = require('assert');
var request = require('supertest');
var app = require('../../app.js');

var server = app.listen(app.get('port'));

suite("Get basic routes", function () {
    test("Get '/' and return \"Hello World!\".", function (done) {
        request(server).get('/').end(function (err, res) {
            assert.equal(res.status, 200);
            assert.equal(res.body, "Hello World!");
        });
        done();
    });

    test("Get '/ProbablyNothingEverHere' and return \"An Error has Occurred\".", function (done) {
        request(server).get('/ProbablyNothingEverHere').end(function (err, res) {
            assert.equal(res.status, 500);
            assert.equal(res.body, "An Error has Occurred");
        });
        done();
    });
});